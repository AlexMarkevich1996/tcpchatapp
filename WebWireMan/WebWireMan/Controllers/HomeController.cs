﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebWireMan.Models;

namespace WebWireMan.Controllers
{
    public class HomeController : Controller
    {
       
        ServiceContext db;

        public HomeController(ServiceContext context)
        {
            db = context;
        }
        
        public IActionResult Index()
        {
            return View(db.Services.ToList());
        }

        [HttpGet]
        public IActionResult Buy(int id)
        {
            ViewBag.ServiceId = id;
            return View();
        }

        [HttpPost]
        public string Buy(Order order)
        {
            db.Orders.Add(order);
            db.Entry(order.Service).State = Microsoft.EntityFrameworkCore.EntityState.Unchanged;
            db.SaveChanges();
            return "Спасибо," + order.User + ", за заказ!";
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
