﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebWireMan.Models;

namespace WebWireMan
{
    public static class SampleData
    {
        public static void Initialize(ServiceContext context)
        {
            if(!context.Services.Any())
            {
                context.Services.AddRange(
                    new Service
                    {
                        Name = "Монтаж проводки",
                        Price = "600$"
                    },
                    new Service
                    {
                        Name = "Навеска люстр",
                        Price = "500$"
                    },
                    new Service
                    {
                        Name = "Обнаружение неисправностей",
                        Price = "400$"
                    }
                 );
                context.SaveChanges();
            }
        }
    }
}
