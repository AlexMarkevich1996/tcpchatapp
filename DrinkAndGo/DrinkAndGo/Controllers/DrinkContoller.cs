﻿using DrinkAndGo.Data.Interfaces;
using DrinkAndGo.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinkAndGo.Controllers
{
    public class DrinkContoller : Controller
    {
        private readonly ICategoryRepository _categoryRepository;

        private readonly IDrinkRepository _drinkRepository;


        public DrinkContoller(ICategoryRepository categoryRepository, IDrinkRepository drinkRepository)
        {
            _categoryRepository = categoryRepository;

            _drinkRepository = drinkRepository;
        }

        public ViewResult List()
        {
            ViewBag.Name = "DotNet, How?";

            DrinkListViewModel vm = new DrinkListViewModel();

            vm.Drinks = _drinkRepository.Drinks;

            vm.CurrentCategory = "DrinkCategory";

            

            return View(vm);
        }
    }
}
