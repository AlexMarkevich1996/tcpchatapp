﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatAndSequences
{
    class Sequence
    {
        private static int currentValue = 0;

        private Sequence()
        {
        }

        public static int NextValue()
        {
            currentValue++;
            return currentValue;
        }
    }
}

class Cat
{
    private string name;
    private string color;

    public Cat(string name, string color)
    {
        this.name = name;
        this.color = color;
    }

    public void sayMiau()
    {
        Console.WriteLine("{0}: Miauu", name);
    }
}



class Program
{
    static void Main(string[] args)
    {
        String name = "Cat";
        for (int i = 0; i < 10; i++)
        {
            Cat cat = new Cat(name + Sequence.NextValue(), "Black");
            cat.sayMiau();
        }
        Console.ReadLine();
    }
}
}
