﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYear
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Write year: ");

            var year = int.Parse(Console.ReadLine());

            Console.WriteLine("It is leap year {0} ? {1} ", year, DateTime.IsLeapYear(year));
        }
    }
}
