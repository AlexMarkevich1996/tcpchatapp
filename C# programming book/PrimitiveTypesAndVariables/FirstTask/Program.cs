﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var positiveNum = 0.0;

            var squareRoot = 0.0;

            bool isItPositiveNum = false;

            bool isItInvalidInput = (positiveNum <= 0 || isItPositiveNum != true);



            while (positiveNum <= 0 || isItPositiveNum != true)
            {
                Console.WriteLine("Please, enter positive number: ");

                
                isItPositiveNum = double.TryParse(Console.ReadLine(), out positiveNum);

                if (positiveNum <= 0 || isItPositiveNum != true)
                {
                    Console.WriteLine("Invalide number");

                    Console.WriteLine("Good Bye");
                }

                else
                {
                    squareRoot = Math.Sqrt(positiveNum);

                    Console.WriteLine($"Valide number. Square root is {squareRoot} Good Bye");
                    break;
                }
            }

            

        }
    }
}
