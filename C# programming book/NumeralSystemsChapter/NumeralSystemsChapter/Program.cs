﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumeralSystemsChapter
{
    class Program
    {
        static void Main(string[] args)
        {
                Console.Write("Number: ");


                float number = float.Parse(Console.ReadLine());


                byte[] arr = BitConverter.GetBytes(number);
                Array.Reverse(arr);
                string result = BitConverter.ToString(arr);

                string binary = "";

                for (int i = 0; i < result.Length; i++)
                {
                    switch (result[i])
                    {
                        case '0':
                            binary += "0000";
                            break;
                        case '1':
                            binary += "0001";
                            break;
                        case '2':
                            binary += "0010";
                            break;
                        case '3':
                            binary += "0011";
                            break;
                        case '4':
                            binary += "0100";
                            break;
                        case '5':
                            binary += "0101";
                            break;
                        case '6':
                            binary += "0110";
                            break;
                        case '7':
                            binary += "0111";
                            break;
                        case '8':
                            binary += "1000";
                            break;
                        case '9':
                            binary += "1001";
                            break;
                        case 'A':
                            binary += "1010";
                            break;
                        case 'a':
                            binary += "1010";
                            break;
                        case 'B':
                            binary += "1011";
                            break;
                        case 'b':
                            binary += "1011";
                            break;
                        case 'C':
                            binary += "1100";
                            break;
                        case 'c':
                            binary += "1100";
                            break;
                        case 'D':
                            binary += "1101";
                            break;
                        case 'd':
                            binary += "1101";
                            break;
                        case 'E':
                            binary += "1110";
                            break;
                        case 'e':
                            binary += "1110";
                            break;
                        case 'F':
                            binary += "1111";
                            break;
                        case 'f':
                            binary += "1111";
                            break;
                        default:
                            break;
                    }
                }

                Console.Write("Result:");
                for (int i = 0; i < binary.Length; i++)
                {
                    Console.Write(binary[i]);
                    if (i == 0 || i == 8)
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            
        }



        public static string ConvertDecimalToBinary(int num)  /**********************Task 1 and 4 ***********/
        {

            int decimalNumber = num;


            int remainder;

            string result = string.Empty;

            while (decimalNumber > 0)
            {
                remainder = decimalNumber % 2;

                decimalNumber /= 2;

                result = remainder.ToString() + result;
            }

            return result;

        }

        public static string ConvertDecimalToHex(string decimalNum)
        {
            string decimalNumber = decimalNum;

            int number = int.Parse(decimalNumber);

            string hex = number.ToString("X");

            return hex;
        }


        public static string ConvertBinaryToHex(string strBinary)  /*****************************Task 2****************/
        {
            string hexValue = Convert.ToInt64(strBinary, 2).ToString("X");

            return hexValue;
        }


        public static int ConvertBinaryToDecimal(string strBinary)/***************Task 2 and 5*************/
        {
            int decimalNumber = Convert.ToInt32(strBinary, 2);

            return decimalNumber;
        }


        public static string ConvertHexToBinary(string hexValue)/****************Task 3*************/
        {
            string binaryVal = "";

            binaryVal = Convert.ToString(Convert.ToInt32(hexValue, 16), 2);

            return binaryVal;
        }


        public static int ConvertHexToDecimal(string hexValue)  /****************Task 3*************/
        {
            int decimalNum = Convert.ToInt32(hexValue, 16);

            return decimalNum;
        }

        public static int ConvertBinaryToDecimalByHornerScheme(string binaryNum) /**************Task 10************/
        {
            int decimalNum = 0;

            string binary = binaryNum;

            int length = binary.Length;

            int power = length - 1;

            for (int i = 0; i < length; i++)
            {
                decimalNum += (int)(int.Parse(binary[i].ToString()) * Math.Pow(2, power));
                power--;
            }

            return decimalNum;
        }


        public static int RomanToArabicDigits(string romanDigit) /************Task 10**************/
        {
            int result = 0;

            String s = romanDigit;
            string[] chars = s.Select(c => c.ToString()).ToArray();

            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] == "m" || chars[i] == "M") result += 1000;
                if (chars[i] == "d" || chars[i] == "D") result += 500;

                if (chars[i] == "c" || chars[i] == "C")
                {
                    result += 100;
                }
                if (chars[i] == "l" || chars[i] == "L") result += 50;

                if (chars[i] == "x" || chars[i] == "X")
                {
                    result += 10;
                }
                if (chars[i] == "v" || chars[i] == "V") result += 5;
                if (chars[i] == "i" || chars[i] == "I")
                {
                    result++;
                }
            }

            return result;
        }

        public static string ArabicToRomanDigits(string arabicDigits)
        {
            String result = "";
           
            int i = Convert.ToInt32(arabicDigits);
            int thousands = i / 1000;
            int hundreds = (i / 100) % 10;
            int tens = (i / 10) % 10;
            int ones = i % 10;

            switch (thousands)
            {
                case 1: result += "M"; break;
                case 2: result += "MM"; break;
                case 3: result += "MMM"; break;
            }

            switch (hundreds)
            {
                case 1: result += "C"; break;
                case 2: result += "CC"; break;
                case 3: result += "CCC"; break;
                case 4: result += "CD"; break;
                case 5: result += "D"; break;
                case 6: result += "DC"; break;
                case 7: result += "DCC"; break;
                case 8: result += "DCCC"; break;
                case 9: result += "CM"; break;
            }

            switch (tens)
            {
                case 1: result += "X"; break;
                case 2: result += "XX"; break;
                case 3: result += "XXX"; break;
                case 4: result += "XL"; break;
                case 5: result += "L"; break;
                case 6: result += "LX"; break;
                case 7: result += "LXX"; break;
                case 8: result += "LXXX"; break;
                case 9: result += "XC"; break;
            }

            switch (ones)
            {
                case 1: result += "I"; break;
                case 2: result += "II"; break;
                case 3: result += "III"; break;
                case 4: result += "IV"; break;
                case 5: result += "V"; break;
                case 6: result += "VI"; break;
                case 7: result += "VII"; break;
                case 8: result += "VIII"; break;
                case 9: result += "IX"; break;
            }

            return result;
        }

        public static string ConvertTo3NumberalSystems()
        {
            int s, d;

            Console.Write("Enter N: ");

            string number = Console.ReadLine();

            do
            {
                Console.Write("Enter S (S == 2, 8, 10 or 16): ");

                s = Int32.Parse(Console.ReadLine());
            }
            while (s != 2 && s != 8 && s != 10 && s != 16);

            do
            {
                Console.Write("Enter D (D == 2, 8, 10 or 16): ");

                d = Int32.Parse(Console.ReadLine());
            }
            while (d != 2 && d != 8 && d != 10 && d != 16);

            number = Convert.ToString(Convert.ToInt32(number, s), d);

            return number;
        }
    }
}

