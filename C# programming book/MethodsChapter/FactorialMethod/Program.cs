﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace FactorialMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 100; i++)
            {
                Factorial(i);
            }
        }
        static void Factorial(int number)
        {
            int num = number;
            BigInteger factorial = 1;
            do
            {
                factorial *= number;
                number--;
            } while (number > 0);
            Console.WriteLine("\n{0}!={1}", num, factorial);
        }
    }
}
