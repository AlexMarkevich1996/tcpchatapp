﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsChapter
{
    class Program
    {
        static void Main(string[] args)
        {
            BiggerNeighbors.StartBiggerNeighbors();
        }

        public static void GetName(string name, string surName)// #1
        {

            Console.WriteLine($"Hello, {name} {surName}!");

            Console.ReadLine();
        }


        public static int GetMaxNumber(int first, int second) // #2
        {
            return first > second ? first : second;
        }


        public static void GetNameOfLastDigit(int num) // #3
        {
            int lastDigit = num % 10;

            switch (lastDigit)
            {
                case 1:
                    Console.WriteLine("One");
                    break;

                case 2:
                    Console.WriteLine("Two");
                    break;

                case 3:
                    Console.WriteLine("Three");
                    break;

                case 4:
                    Console.WriteLine("Four");
                    break;

                case 5:
                    Console.WriteLine("Five");
                    break;

                case 6:
                    Console.WriteLine("Six");
                    break;

                case 7:
                    Console.WriteLine("Seven");
                    break;

                case 8:
                    Console.WriteLine("Eight");
                    break;

                case 9:
                    Console.WriteLine("Nine");
                    break;

                case 10:
                    Console.WriteLine("Ten");
                    break;
            }


        }

        static void NumberAppearsInArray()//*********************************************************** #4
        {
            Console.Write("Length of array: ");

            int length = int.Parse(Console.ReadLine());

            FillArray(length);
        }

        static void FillArray(int length)
        {
            int[] arr = new int[length];

            for (int i = 0; i < length; i++)
            {
                Console.Write("Fill array[{0}]: ", i + 1);

                arr[i] = int.Parse(Console.ReadLine());
            }
            Console.Write("Number to find: ");

            int number = int.Parse(Console.ReadLine());

            NumberAppears(arr, number);
        }

        static void NumberAppears(int[] arr, int number)
        {
            int count = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == number)
                {
                    count++;
                }
            }
            Console.WriteLine("{0} appears in array {1} times.", number, count);//*****************************
        }


    }
}
