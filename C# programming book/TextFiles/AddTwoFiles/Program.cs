﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AddTwoFiles
{
    class Program
    {//Write a program that joins two text files and records the results in a third file.
        static void Main(string[] args)
        {
            string firstFileName = "note.txt";
            string secondFileName = "text.txt";
            string textFirstFile = null;
            string textSecondFile = null;
            try
            {
                StreamReader reader = new StreamReader(firstFileName);

                using (reader)
                {
                    textFirstFile = reader.ReadToEnd();

                }

                StreamReader readerSecond = new StreamReader(secondFileName);

                using (readerSecond)
                {
                   textSecondFile = readerSecond.ReadToEnd();

                }

                StreamWriter writer = new StreamWriter("thirdFile.txt");

                string allText = textFirstFile + textSecondFile;

                using (writer)
                {
                    writer.Write(allText);
                }
            }
            catch (FileNotFoundException)
            {
                Console.Error.WriteLine(
                "Can not find file {0}.", firstFileName);
            }
            catch (DirectoryNotFoundException)
            {
                Console.Error.WriteLine(
                "Invalid directory in the file path.");
            }
            catch (IOException)
            {
                Console.Error.WriteLine(
                "Can not open the file {0}", firstFileName);
            }
        }
    }
}
