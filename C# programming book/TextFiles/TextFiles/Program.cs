﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TextFiles
{
    class Program
    {
        //Write a program that reads a text file and prints its odd lines on the console.
        static void Main(string[] args)
        {
            string fileName = "note.txt";

            try
            {
                StreamReader reader = new StreamReader(fileName);

                using (reader)
                {
                    int lineNumber = 0;

                    string line = reader.ReadLine();

                    while (line != null)
                    {
                        lineNumber++;
                        if(lineNumber % 2 != 0)
                        {
                            Console.WriteLine($"Line {lineNumber}: {line}");
                            line = reader.ReadLine();
                        }
                        
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.Error.WriteLine(
                "Can not find file {0}.", fileName);
            }
            catch (DirectoryNotFoundException)
            {
                Console.Error.WriteLine(
                "Invalid directory in the file path.");
            }
            catch (IOException)
            {
                Console.Error.WriteLine(
                "Can not open the file {0}", fileName);
            }
        }
    }
}
