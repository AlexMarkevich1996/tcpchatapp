﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecursionChapter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(FactorialByIteration(10));
        }

        static long Fibonacci(int n)
        {
            if (n <= 2)
            {
                return 1;
            }


            return Fibonacci(n - 1) + Fibonacci(n - 2);
        }

        static decimal Factorial(int n)
        {
            if (n == 0)
            {
                return 1;
            }

            return n * Factorial(n - 1);
        }

        static decimal FactorialByIteration(int n)
        {
            decimal number = 1;

            for(int i = 1; i <= n; i++)
            {
                number *= i;
            }

            return number;
        }



    }
}
