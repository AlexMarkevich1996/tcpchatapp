﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindAllSubsetsOfSet
{
    class Program
    {
        static string[] strings, str;

        static int length;


        static void Main(string[] args)
        {
            Console.Write("Enter words length: ");

            length = Int32.Parse(Console.ReadLine());

            strings = new string[length];

            InitElementsOfArray();


        }


        static void InitElementsOfArray()
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("Enter {0} word: ", i + 1);

                strings[i] = Console.ReadLine();
            }

            for (int i = 0; i <= length; i++)
            {
                str = new string[length];

                Cycle(0, 0, i);
            }
        }


        static void Cycle(int iter, int index, int k)
        {
            if (iter == k)
            {
                for (int i = 0; i < length; i++)
                {
                    Console.WriteLine("({0})", str[i]);
                }
                return;
            }

            for (int i = index; i < strings.Length; i++)
            {
                str[iter] = strings[i];

                Cycle(iter + 1, i + 1, k);
            }
        }

        
    }
}
