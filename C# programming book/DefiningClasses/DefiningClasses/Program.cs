﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DefiningClasses
{
    
    class StartProgram
    {
       

        static void Main()
        {
            //Define a class Student, which contains the following information about students: full name, course, subject, university, e-mail and phone number.

            Student sam = new Student("Alex", "Markevich", 6, "BSMU", "sanczes0@gmail.com", "375291471852");

            sam.GetInfo();
        }


    }

    public class Student
    {
        public string Name { get; set; }

        public string SurName { get; set; }

        public int Course { get; set; }

        public string University { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }


        public Student(string name, string surName, int course, string university, string email, string phoneNum)
        {
            Name = name;

            SurName = surName;

            Course = course;

            University = university;

            Email = email;

            PhoneNumber = phoneNum;
        }

        public void GetInfo()
        {
            Console.WriteLine($"Full Name : {Name} {SurName}");

            Console.WriteLine($"Course : {Course}");

            Console.WriteLine($"University : {University}");

            Console.WriteLine($"Email : {Email}");

            Console.WriteLine($"Phone number : {PhoneNumber}");
        }
    }
    
    
}
