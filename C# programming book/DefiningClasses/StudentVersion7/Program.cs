﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentVersion7
{
    class TestProgram
    {

        //Добавьте статический метод в класс StudentTest, который создает несколько объектов типа Студент и сохраняет их в статическом поле. 
        //Создайте статическое свойство класса для доступа к ним. Напишите тестовую программу, которая отображает информацию о них в консоле

        public static int NumberOfCreatedObjects { get; set; }

        static void Main(string[] args)
        {
            TestStudent();
        }

        public static void TestStudent()
        {
            Student[] students = new Student[10];

            Console.WriteLine($"\nNumber of created objects: {++NumberOfCreatedObjects}\n");

            for(int i = 0; i < students.Length; i++)
            {
                students[i] = new Student();

                Console.WriteLine(students[i].GetInfo());

                Console.WriteLine($"\nNumber of created objects: {++NumberOfCreatedObjects}\n");
            }
        }
    }
}
