﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentVersion7
{
    public class Student
    {
        public string Name { get; set; }

        public string SurName { get; set; }

        public int Course { get; set; }

        public string University { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        


        public Student() : this("Unknown", "Unknown", 0, "Unknown", "Unknown", "Unknown")
        {

        }

        public Student(string name) : this(name, "", 0, "Unknown", "Unknown", "Unknown")
        {

        }

        public Student(string name, string surName) : this(name, surName, 0, "Unknown", "Unknown", "Unknown")
        {

        }

        public Student(string name, string surName, int course) : this(name, surName, course, "Unknown", "Unknown", "Unknown")
        {

        }

        public Student(string name, string surName, int course, string university) : this(name, surName, course, university, "Unknown", "Unknown")
        {

        }

        public Student(string name, string surName, int course, string university, string email) : this(name, surName, course, university, email, "Unknown")
        {

        }

        public Student(string name, string surName, int course, string university, string email, string phoneNum)
        {
            Name = name;

            SurName = surName;

            Course = course;

            University = university;

            Email = email;

            PhoneNumber = phoneNum;
        }

        public void GetInfo()
        {
            Console.WriteLine($"Full Name : {Name} {SurName}");

            Console.WriteLine($"Course : {Course}");

            Console.WriteLine($"University : {University}");

            Console.WriteLine($"Email : {Email}");

            Console.WriteLine("Phone number : {0}", PhoneNumber);

            
        }
    }
}
