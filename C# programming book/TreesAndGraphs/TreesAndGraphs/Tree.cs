﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreesAndGraphs
{
    public class Tree<T>
    {
        //The root of the tree
        private TreeNode<T> root;

        ///<summary>Constracts the tree</summary>
        ///<param name="value">the value of the node</param>
        public Tree(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Cannot insert null value");
            }

            root = new TreeNode<T>(value);
        }

        ///<summary>Constructs the tree</summary>
        ///<param name="value">the value of the root node</param>
        ///<param name="children">the children of root node</param>
        public Tree(T value, params Tree<T>[] children) : this(value)
        {
            foreach (Tree<T> child in children)
            {
                Root.AddChild(child.root);
            }
        }


        ///<summary>The root node or null if the tree is empty</summary>
        public TreeNode<T> Root { get => root; set => root = value; }


        ///<summary>Traverses and prints tree in Depth-First-Search (DFS) manner</summary>
        ///<param name="root">the root of the tree to be traversed</param>
        ///<param name="spaces">the spaces used for the representation of the parent-child relation</param>
        private void PrintDFS(TreeNode<T> root, string spaces)
        {
            if (root == null)
            {
                return;
            }

            Console.WriteLine(spaces + root.Value);

            TreeNode<T> child = null;

            for (int i = 0; i < root.ChildrenCount; i++)
            {
                child = root.GetChild(i);

                PrintDFS(child, spaces + "  ");
            }
        }

        /// <summary>
        /// Traverses and prints the tree in the Depth-First Search (DFS) manner
        /// </summary>
        public void TraverseDFS()
        {
            PrintDFS(root, string.Empty);
        }
    }
}
