﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreesAndGraphs
{
    /// <summary>
    /// Represents a tree node
    /// </summary>
    /// <typeparam name="T">
    /// the types of the values in nodes
    /// </typeparam>
    public class TreeNode<T>
    {
        /// <summary>
        /// The value of the node
        /// </summary>
        public T Value { get; set; }


        public bool HasParent { get; set; }


        public List<TreeNode<T>> Children { get; set; }


        ///<summary>Constructs a tree node</summary>
        ///<param name="value">the value of the node</param>
        public TreeNode(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Cannot insert null value!");
            }
            this.Value = value;

            Children = new List<TreeNode<T>>();
        }


        ///<summary>Number of node's children</summary>
        public int ChildrenCount
        {
            get
            {
                return Children.Count;
            }
        }


        ///<summary>Add children to the node</summary>
        ///<param name="child">Child to be added</param>
        public void AddChild(TreeNode<T> child)
        {
            if (child == null)
            {
                throw new ArgumentNullException("Cannot insert null value!");
            }

            if (child.HasParent)
            {
                throw new ArgumentException("The node already has a parent");
            }

            child.HasParent = true;

            Children.Add(child);
        }


        ///<summary>Get the child of the node at given index</summary>
        ///<param name="index">the index of the desired child</param>
        ///<returns>The child on the given position</returns>
        public TreeNode<T> GetChild(int index)
        {
            return Children[index];
        }
    }
}
