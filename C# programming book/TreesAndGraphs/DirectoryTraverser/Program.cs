﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DirectoryTraverser
{


    class Program
    {
        private static void TraverseDir(DirectoryInfo dir, string spaces)
        {
            try
            {
                // Visit the current directory
                Console.WriteLine(spaces + dir.FullName);
                DirectoryInfo[] children = dir.GetDirectories();
                // For each child go and visit its sub-tree
                foreach (DirectoryInfo child in children)
                {
                    TraverseDir(child, spaces + " ");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                
            }
            
            
        }

        static void TraverseDir(string directoryPath)
        {
            TraverseDir(new DirectoryInfo(directoryPath), string.Empty);
        }

        static void Main(string[] args)
        {
            TraverseDir("D:\\");
        }
    }
}
