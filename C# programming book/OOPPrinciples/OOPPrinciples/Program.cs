﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPPrinciples
{

    class Program
    {
        interface IAccount
        {
            int CurrentSum { get; }

            void Put(int sum);

            void Withdraw(int sum);
        }

        interface IClient
        {
            string Name { get; set; }
        }

        class Client : IClient, IAccount
        {
            public int Sum { get; set; }

            public string Name { get; set; }

            public Client(string name, int sum)
            {
                Name = name;

                Sum = sum;
            }

            public int CurrentSum { get { return Sum; } }

            public void Put(int sum)
            {
                Sum += sum;
            }

            public void Withdraw(int sum)
            {
                if(Sum >= sum)
                {
                    Sum -= sum;
                }
            }
        }

        static void Main(string[] args)
        {
            Client client = new Client("Tom", 200);
            client.Put(30);

            Console.WriteLine(client.CurrentSum);

            client.Withdraw(100);
            Console.WriteLine(client.CurrentSum);
        }
    }
}
