﻿using System;

using Newtonsoft.Json;

using ChatApp.ServerPartOfChat;
using ChatApp.ClientPartOfChat;

namespace ChatRoom
{
    class Program
    {
        private const string ipServer = "127.0.0.1";

        public static int serverPort => 7000;

        static void Main(string[] args)
        {
            if (GetApplicationWorkMode() == AppMode.Client)
            {
                ClientApp.StartWork();
            }
            else
            {
                Server.StartListening();
            }

            Console.WriteLine("\nPress any key for exit...");
            Console.ReadKey();
        }


        private static AppMode GetApplicationWorkMode()
        {
            Console.Write("Please, choose app mode (for client press 1, for server press 2): ");

            bool? isClient = null;

            do
            {
                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        isClient = true;
                        break;

                    case '2':
                        isClient = false;
                        break;

                    default:
                        Console.Write("\nPlease, try one more time. Only 1 or 2 are avaliable: ");
                        break;
                }

            }
            while (isClient.HasValue == false);

            Console.WriteLine(
                $"\nThanks, your choice is {(isClient.Value ? "client" : "server")}." + Environment.NewLine + 
                $"Have a nice day!\n");

            return isClient.HasValue && isClient.Value ? AppMode.Client : AppMode.Server;
        } 


        private enum AppMode { Client, Server }

    }
}