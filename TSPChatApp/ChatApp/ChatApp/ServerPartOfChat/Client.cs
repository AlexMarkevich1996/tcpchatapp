﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ChatApp.ServerPartOfChat
{
    public class ClientConnection
    {
        protected internal string Id { get; private set; }
        private NetworkStream StreamFromUserToServer { get; }
        public string Username;

        TcpClient TcpClient = new TcpClient();
        
        public ClientConnection(TcpClient tcpClient)
        {
            Id = Guid.NewGuid().ToString();

            TcpClient = tcpClient;

            StreamFromUserToServer = TcpClient.GetStream();
        }
        
        public void StartListening()
        {
            Username = GetMessage();

            Server.SendMessageForAll($"{Username} entered chat!", this);
            
            while (true)
            {
                var message = $"{Username}: {GetMessage()}";

                Console.WriteLine(message);
                
                Server.SendMessageForAll(message, this);
            }
        }



        public void SendMessageToMe(string message)
        {
            var json = JsonConvert.SerializeObject(message);

            SendMessageToMe(Encoding.UTF8.GetBytes(json));
        }
        
        private void SendMessageToMe(byte[] message)
        {
            StreamFromUserToServer.Write(message, 0, message.Count());
        }



        private string GetMessage()
        {
            
            var myReadBuffer = new byte[1024];
            var myCompleteMessage = string.Empty;
            int numberOfBytesRead = 0;

            do
            {
                numberOfBytesRead = StreamFromUserToServer.Read(myReadBuffer, 0, myReadBuffer.Length);

                myCompleteMessage += Encoding.UTF8.GetString(myReadBuffer, 0, numberOfBytesRead);
            }
            while (StreamFromUserToServer.DataAvailable);

            var message = JsonConvert.DeserializeObject<string>(myCompleteMessage);
            
            return message;
        }


        protected internal void Close()
        {
            StreamFromUserToServer?.Close();
            TcpClient?.Close();
        }
    }
}
