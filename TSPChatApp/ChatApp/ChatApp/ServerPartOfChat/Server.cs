﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatApp.ServerPartOfChat
{
    public class Server
    {
        static TcpListener TcpListener = new TcpListener(IPAddress.Any, 2000);
        static List<ClientConnection> ClientConnections = new List<ClientConnection>();

        
        public static void StartListening()
        {
            try
            {
                TcpListener.Start();

                Console.WriteLine("\nServer started. Waiting for connections...");

                while (true)
                {
                    var tcpClient = TcpListener.AcceptTcpClient();

                    var clientConnection = new ClientConnection(tcpClient);

                    ClientConnections.Add(clientConnection);
                    
                    new Thread(()=> 
                    {
                        try
                        {
                            clientConnection.StartListening();
                        }
                        catch (Exception ex)
                        {
                            RemoveClient(clientConnection);

                            SendMessageForAll($"{clientConnection.Username} has left our chat.", clientConnection);

                            Console.WriteLine($"\nClient {clientConnection.Username} suddenly stopped working.\nException:\n{ex}\n");
                        }
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                DisconnectAllClients();
            }
        }


        public static void SendMessageForAll(string message, ClientConnection author)
        {
            ClientConnections.ForEach(client =>
            {
                if (client.Id != author.Id)
                {
                    client.SendMessageToMe(message);
                }
            });
        }

        public static void BroadcastMessage(string message, string id)
        {
            ClientConnections.ForEach(client => 
            {
                if (client.Id != id)
                {
                    client.SendMessageToMe(message);
                }
            });
        }

        protected static void RemoveClient(ClientConnection client)
        {
            client?.Close();

            ClientConnections.Remove(client);
        }
        
        private static void DisconnectAllClients()
        {
            TcpListener.Stop();

            ClientConnections.ForEach(client => client?.Close());

            Environment.Exit(0); 
        }
    }
}
