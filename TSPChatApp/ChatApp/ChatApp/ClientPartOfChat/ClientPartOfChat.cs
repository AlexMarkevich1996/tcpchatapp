﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatApp.ClientPartOfChat
{
    public class ClientApp
    {
        static string Username;
        private const string host = "195.222.75.102";
        private const int port = 2000;
        static TcpClient TcpClient;
        static NetworkStream StreamFromUserToServer;

        static void FindServer()
        {
            TcpClient = new TcpClient();
            TcpClient.Connect(host, port);
            StreamFromUserToServer = TcpClient.GetStream();
        }

        public static void StartWork()
        {
            try
            {
                FindServer();
                StartWorkPrivate();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception:\n{ex}\n");
                StreamFromUserToServer?.Close();
                TcpClient?.Close();
            }
        }


        static void StartWorkPrivate()
        {
            Console.Write("Enter your name: ");

            Username = Console.ReadLine();

            SendMessage(Username);

            StartReceiveMessages();

            StartSendingMessages();
        }
        
        static void StartSendingMessages()
        {
            new Thread(() =>
            {
                while (true)
                {
                    SendMessage(Console.ReadLine());
                }
            }).Start();
        }

        static void SendMessage(string message)
        {
            var json = JsonConvert.SerializeObject(message);

            var data = Encoding.UTF8.GetBytes(json);

            StreamFromUserToServer.Write(data, 0, data.Length);
        }

        static void StartReceiveMessages()
        {
            new Thread(() =>
            {
                while (true)
                {
                    var myCompleteByteArray = new byte[] { };
                    
                    do
                    {
                        var myReadBuffer = new byte[1024];

                        var numberOfBytesRead = StreamFromUserToServer.Read(myReadBuffer, 0, myReadBuffer.Length);

                        myCompleteByteArray = Combine(myCompleteByteArray, myReadBuffer);
                    }
                    while (StreamFromUserToServer.DataAvailable);

                    var myCompleteMessage = Encoding.UTF8.GetString(myCompleteByteArray);

                    var message = JsonConvert.DeserializeObject<string>(myCompleteMessage);
                    
                    if (string.IsNullOrWhiteSpace(message) == false)
                    {
                        Console.WriteLine(message);
                    }
                }
            }).Start();
        }

        static byte[] Combine(params byte[][] arrays)
        {
            byte[] rv = new byte[arrays.Sum(a => a.Length)];
            int offset = 0;
            foreach (byte[] array in arrays)
            {
                System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            return rv;
        }
    }
}
