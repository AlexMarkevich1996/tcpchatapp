﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace ChatRoom
{
    class StartProgramOfServer
    {
        static void Main(string[] args)
        {
            new Thread(new Server().Listen).Start();
        }
    }
}
