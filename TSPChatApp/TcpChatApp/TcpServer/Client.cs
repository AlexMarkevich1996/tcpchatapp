﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom
{
    public class Client
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream StreamFromUserToServer { get; private set; }
        string userName;
        TcpClient client = new TcpClient();


        public Client(TcpClient tcpClient)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            Server.AddConnection(this);
        }

        
        public void Process()
        {
            try
            {
                StreamFromUserToServer = client.GetStream();


                string message = GetMessage();
                userName = message;

                //message = userName + " come into chat";



                Server.BroadcastMessage(message, this.Id);

                Console.WriteLine(message);


                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        message = userName + message;
                        Console.WriteLine(message);
                        Server.BroadcastMessage(message, this.Id);
                    }
                    catch(Exception ex)
                    {
                        string jsonText = JsonConvert.SerializeObject("leave chat");

                        string messageUserLeaveChat = userName + jsonText;
                        
                        Console.WriteLine(messageUserLeaveChat);
                        Server.BroadcastMessage(messageUserLeaveChat, this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                //server.RemoveConnection(this.Id);
            }
        }


        private string GetMessage()
        {
            byte[] data = new byte[8];


            var messageJson = string.Empty;
            int bytes = 0;
            do
            {
                bytes = StreamFromUserToServer.Read(data, 0, data.Length);
                string jsonData = JsonConvert.SerializeObject(bytes);

                string message = JsonConvert.SerializeObject(" come into chat");

                messageJson = data + message;

            }
            while (StreamFromUserToServer.DataAvailable);

            return messageJson;
        }


        protected internal void Close()
        {

            StreamFromUserToServer?.Close();
            client?.Close();
        }
    }
}
