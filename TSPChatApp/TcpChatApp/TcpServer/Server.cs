﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatRoom
{
    public class Server
    {
        TcpListener tcpListener = new TcpListener(IPAddress.Any, 8888);
        static List<Client> clients = new List<Client>();

        public static void AddConnection(Client clientObject)
        {
            clients.Add(clientObject);
        }

        protected static void RemoveConnection(string id)
        {

            var client = clients.FirstOrDefault(c => c.Id == id);

            // and delete him
            if (client != null)
            {
                clients.Remove(client);
                client.Close();
            }

        }

        // listenning of input connections
        public  void Listen()
        {
            try
            {
                tcpListener.Start();

                Console.WriteLine("Server started. Waiting connections...");

                while (true)
                {
                    var tcpClient = tcpListener.AcceptTcpClient();
                    
                    var client = new Client(tcpClient);

                    new Thread(client.Process).Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                DisconnectAllClients();
            }
        }
   
        // message broadcast to all online users
        public static void BroadcastMessage(string message, string id)
        {
            var json = JsonConvert.SerializeObject(message);

            var data = Encoding.UTF8.GetBytes(json);

            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id != id) // if client id is not equal of id sending
                {
                    clients[i].StreamFromUserToServer.Write(data, 0, data.Length); //data transmission
                }
            }
        }
        // disconnect of all clients
        protected internal void DisconnectAllClients()
        {
            tcpListener.Stop(); //stop server

            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Close(); //log off all clients
            }
            Environment.Exit(0); // end
        }

        
    }
}
