﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;

namespace ChatClient
{
    class StartProgramOfServer
    {
        static string userName;
        private const string host = "127.0.0.1";
        private const int port = 8888;
        static TcpClient client;
        static NetworkStream streamFromUserToServer;

        static void Main(string[] args)
        {
            Console.Write("Enter your name: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                client.Connect(host, port); //client is online
                streamFromUserToServer = client.GetStream(); // receive thread

                
                var json = JsonConvert.SerializeObject(userName);
                var data = Encoding.UTF8.GetBytes(json);       //var json = JsonConvert.SerializeObject(message);
                                                                                    //var data = Encoding.UTF8.GetBytes(json);



                streamFromUserToServer.Write(data, 0, data.Length);

                // run new thread for receiving data
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //start thread
                Console.WriteLine("Welcom " + userName + "!");
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // send messages
        static void SendMessage()
        {
            Console.WriteLine("Enter message: ");

            while (true)
            {
                string message = Console.ReadLine();

                string json = JsonConvert.SerializeObject(message);
                byte[] data = JsonConvert.DeserializeObject<byte[]>(json);
                streamFromUserToServer.Write(data, 0, data.Length);
            }
        }
        // receive messages
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; // for receiving data

                    var message = string.Empty;
                    int bytes = 0;
                    do
                    {
                        bytes = streamFromUserToServer.Read(data, 0, data.Length);
                        message = JsonConvert.SerializeObject(bytes);
                    }
                    while (streamFromUserToServer.DataAvailable);

                    Console.WriteLine(message);//output messages
                }
                catch
                {
                    Console.WriteLine("Connection lost!"); 
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
                streamFromUserToServer?.Close();
                client?.Close();
            Environment.Exit(0); 
        }
    }
}
