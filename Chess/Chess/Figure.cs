﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessLibrary
{
    enum Figure
    {
        none,

        WhiteKing = 'K',

        WhiteQueen = 'Q',

        WhiteRook = 'R',

        WhiteBishop = 'B',

        WhiteKnight = 'K',

        WhitePawn = 'P',


        BlackeKing = 'k',

        BlackeQueen = 'q',

        BlackeRook = 'r',

        BlackeBishop = 'b',

        BlackeKnight = 'k',

        BlackePawn = 'p',
    }
}
