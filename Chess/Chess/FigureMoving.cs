﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessLIbrary;

namespace ChessLibrary
{
    class FigureMoving
    {
        public Figure Figure { get; private set; }

        public Square FromSquare { get; private set; }

        public Square ToSquare { get; private set; }

        public Figure Promotion { get; private set; }


        public FigureMoving(FigureOnSquare fs, Square to, Figure promotion = Figure.none)
        {
            Figure = fs.Figure;

            ToSquare = to;

            Promotion = promotion;
        }

        public FigureMoving(string move)
        {
            Figure = (Figure)move[0]
        }

    }
}
