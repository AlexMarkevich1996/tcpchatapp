﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessLibrary
{
    struct Square
    {

        public int X { get; private set; }

        public int Y { get; private set; }

        public static Square missCoordinate = new Square(-1, -1);

        public Square(int x, int y)
        {
            X = x;

            Y = y;
        }

        public Square(string coordinate)
        {
            bool isCorrectCoordinate = (coordinate.Length == 2) &&
                                       (coordinate[0] >= 'a' && coordinate[0] <= 'h') &&
                                       (coordinate[1] >= 1 && coordinate[1] <= 8);

            if (isCorrectCoordinate)
            {
                X = coordinate[0] - 'a';

                Y = coordinate[1] - 1;
            }
            else
            {
                this = missCoordinate;
            }
        }

        public bool IsPieceOnBoard()
        {
            bool isOnBoard = (X >= 0 && X < 8) && (Y >= 0 && Y < 8);

            return isOnBoard;
        }
    }
}
