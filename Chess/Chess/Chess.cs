﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessLibrary
{
    public class Chess
    {
        public string Fen { get; private set; }

        

        public Chess() : this("Null")
        {

        }

        public Chess(string fen)
        {
            Fen = fen;
        }

        public Chess Move(string move)
        {
            Chess nextChess = new Chess(Fen);

            return nextChess;
        }

        public char GetFigureAt(int x, int y)
        {
            return '.';
        }
    }
}
